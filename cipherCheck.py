#!/usr/bin/python

import argparse
import requests as req
import bs4
import json

wrongCiphers = []
safeCiphers = []
cipherStatus = ['insecure', 'weak']

parser = argparse.ArgumentParser(description='It is useful to read the ciphers obtained with nmap and compared with ciphersuite')
parser.add_argument('--message', '-m', help="Take it easy %(prog)s is here ;)", action="store_true")
parser.add_argument('--extract', '-e', help="Extract weak algorithms from nmap file")

def getAlgos(path):
    f = open(path,'r')
    getal=False
    for x in f:
        if x.endswith("compressors: \n"):
            getal=False
        if getal:
            algorithms = x.split(' ')
            cleanAlgo = max(algorithms, key=len)
            if isWeak(cleanAlgo):
                wrongCiphers.append(cleanAlgo)
            else:
                safeCiphers.append(cleanAlgo)
        if x.endswith(" ciphers: \n"):
            getal=True
    f.close()

def isWeak(algorithm):
    badCipher = False
    resp = req.get("https://ciphersuite.info/api/cs/"+algorithm)
    if resp.status_code == 200:
        d = json.loads(resp.text)
        # this is a double check, you know sometimes we must be paranoic, but we want to avoid false positives
        if d[algorithm]['security'] in cipherStatus:
            badCipher = True
    return badCipher


if __name__ == '__main__':
    args = parser.parse_args()

    if args.extract:
        getAlgos(args.extract)
        if len(wrongCiphers) > 0:
            print("\n\t\________________________/\n\t Weak or Insecure Ciphers\n\t ========================")
            print("Avoid this Ciphers (weak or insecure)")
        for i in wrongCiphers:
            print(i)
        if len(safeCiphers) > 0:
            print("\n\t\____________/\n\t Safe Ciphers\n\t ============")
        for i in safeCiphers:
            print(i)
        
    else:
        print("For help try with  -h or --help")

